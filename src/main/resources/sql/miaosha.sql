-- 创建库
CREATE DATABASE miaosha;
-- 使用库
USE miaosha;

-- 创建用户表
drop table IF EXISTS `user`;
CREATE TABLE `user` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `mobile` varchar(11) DEFAULT '' COMMENT '手机号',
    `nickname` varchar(30) DEFAULT '' COMMENT '用户名',
    `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '密码',
    `salt` varchar(10) NOT NULL COMMENT '加密验证字符',
    `register_date` datetime NOT NULL COMMENT '注册时间',
    `last_login_date` datetime NOT NULL COMMENT '最后登陆时间',
    `login_count` int(11) NOT NULL DEFAULT 1 COMMENT '登陆次数',
    `del_flag` tinyint(1) DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
    `create_by` int(11) DEFAULT '0' COMMENT '创建者',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_by` int(11) DEFAULT '0' COMMENT '更新者',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='登陆用户表';

INSERT INTO `miaosha`.`user`(`mobile`, `nickname`, `password`, `salt`, `register_date`, `last_login_date`, `login_count`) VALUES
('13100001111', '天涯', '77b32a73756dcc6b2f0915d112f6f6a1', '1a2s3d4f', now(), now(), 1);


-- 创建商品表
drop table IF EXISTS `goods`;
CREATE TABLE `goods`(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `goods_name` varchar(20) DEFAULT '' COMMENT '商品名称',
    `goods_title` varchar(64) DEFAULT '' COMMENT '商品标题',
    `goods_img` varchar(255) DEFAULT '' COMMENT '商品的图片',
    `goods_detail` text COMMENT '商品的详情介绍',
    `goods_price` decimal(11,2) DEFAULT '0.00' COMMENT '商品单价',
    `goods_stock` int(11) DEFAULT '0' COMMENT '商品库存,-1表示没有限制',
    `del_flag` tinyint(1) DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
    `create_by` int(11) DEFAULT '0' COMMENT '创建者',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_by` int(11) DEFAULT '0' COMMENT '更新者',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='商品表';

-- 创建秒杀商品表
DROP TABLE IF EXISTS `miaosha_goods`;
CREATE TABLE `miaosha_goods` (
     `id` INT (11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
     `goods_id` INT (11) DEFAULT NULL COMMENT '商品ID',
     `miaosha_price` DECIMAL (11, 2) DEFAULT '0.00' COMMENT '秒杀价',
     `stock_count` INT (11) DEFAULT NULL COMMENT '库存数星',
     `start_date` datetime DEFAULT NULL COMMENT '秒杀开始时间',
     `end_date` datetime DEFAULT NULL COMMENT '秒杀结束时间',
     `del_flag` TINYINT (1) DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
     `create_by` INT (11) DEFAULT '0' COMMENT '创建者',
     `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `update_by` INT (11) DEFAULT '0' COMMENT '更新者',
     `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
     PRIMARY KEY (`id`)
) ENGINE = INNODB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC COMMENT = '秒杀商品表';

-- 创建订单表
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info` (
     `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
     `user_id` INT (11) DEFAULT NULL COMMENT '用户ID',
     `goods_id` INT (11) DEFAULT NULL COMMENT '商品ID',
     `delivery_addr_id` INT (11) DEFAULT NULL COMMENT '收获地址lD',
     `goods_name` VARCHAR (20) DEFAULT NULL COMMENT '商品名称',
     `goods_count` INT (11) DEFAULT '0' COMMENT '商品数量',
     `goods_price` DECIMAL (11, 2) DEFAULT '0.00' COMMENT '商品单价',
     `order_channel` TINYINT (1) DEFAULT '0' COMMENT '1pc,2android,3ios',
     `status` TINYINT (1) DEFAULT '0' COMMENT '订单状态,0新建未支付,1已支付,2已发货,3已收货,4已退款,5已完成',
     `pay_date` datetime DEFAULT NULL COMMENT '支付时间',
     `del_flag` TINYINT (1) DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
     `create_by` INT (11) DEFAULT '0' COMMENT '创建者',
     `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `update_by` INT (11) DEFAULT '0' COMMENT '更新者',
     `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
     PRIMARY KEY ( `id` )
) ENGINE = INNODB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC COMMENT = '订单表';

-- 创建秒杀订单表
DROP TABLE IF EXISTS `miaosha_order`;
CREATE TABLE `miaosha_order` (
     `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
     `user_id` int(11) NOT NULL COMMENT '用户ID',
     `order_id` int(11) NOT NULL COMMENT '订单ID',
     `goods_id` int(11) NOT NULL COMMENT '商品ID',
     `del_flag` TINYINT (1) DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
     `create_by` INT (11) DEFAULT '0' COMMENT '创建者',
     `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `update_by` INT (11) DEFAULT '0' COMMENT '更新者',
     `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
     PRIMARY KEY (id),
     UNIQUE KEY `uk_user_goods` (`user_id`, `goods_id`)
) ENGINE = INNODB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 ROW_FORMAT = DYNAMIC COMMENT = '秒杀订单表';

-- 新增商品
INSERT INTO `goods`(`goods_name`, `goods_title`, `goods_img`, `goods_detail`, `goods_price`, `goods_stock`) VALUES
    ('小米手环', '小米手环', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F050621115156%2F210506115156-5-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com', '支持NFC功能', 199.00, 100),
    ('iPhone', 'iPhone', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fup.enterdesk.com%2Fedpic%2F09%2F3a%2Fbc%2F093abce7b31f4c8ffdbf345375ff4abb.jpg', '系统好用', 8999.00, 200);

-- 新增秒杀商品
INSERT INTO `miaosha_goods` (`goods_id`, `miaosha_price`, `stock_count`, `start_date`, `end_date`) VALUES
    (1, 99.00, 10, now() , DATE_ADD(now(),INTERVAL 10 day)),
    (2, 5999.00, 30, now() , DATE_ADD(now(),INTERVAL 10 day));