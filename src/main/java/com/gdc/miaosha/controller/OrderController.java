package com.gdc.miaosha.controller;


import com.gdc.miaosha.config.UserContext;
import com.gdc.miaosha.entity.OrderInfo;
import com.gdc.miaosha.entity.User;
import com.gdc.miaosha.result.CodeMsg;
import com.gdc.miaosha.result.Result;
import com.gdc.miaosha.service.IGoodsService;
import com.gdc.miaosha.service.IOrderInfoService;
import com.gdc.miaosha.vo.GoodsVO;
import com.gdc.miaosha.vo.OrderDetailVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderInfoService orderInfoService;
    @Autowired
    private IGoodsService goodsService;

    @RequestMapping("/detail")
    @ResponseBody
    public Result<OrderDetailVO> info(@RequestParam("orderId") Integer id) {
        // 判断用户是否登陆
        User user = UserContext.getUser();
        if(Objects.isNull(user)) {
            return Result.error(CodeMsg.SESSION_ERROR);
        }

        // 获取订单
        OrderInfo order = orderInfoService.getById(id);
        if(Objects.isNull(order)) {
            return Result.error(CodeMsg.ORDER_NOT_EXIST);
        }

        // 获取商品信息
        GoodsVO goodsVO = goodsService.getGoodsVoById(order.getGoodsId());
        return Result.success(new OrderDetailVO(order, goodsVO));
    }
}
