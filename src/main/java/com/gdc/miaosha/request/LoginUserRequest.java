package com.gdc.miaosha.request;

import com.gdc.miaosha.validator.IsMobile;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * Description: 登陆用户 Request
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
@Data
public class LoginUserRequest {

	@IsMobile
	@NotBlank
	@ApiModelProperty("手机号")
	private String mobile;

	@NotBlank
	@Length(min = 32, max = 32)
	@ApiModelProperty("密码")
	private String password;

}