package com.gdc.miaosha.request;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Description: 用户修改密码 Request
 * @author: gdc
 * @date: 2022/4/5
 * @version 1.0
 */
@Api(value = "用户修改密码请求参数")
@Data
public class UserModifyPwdRequest {

	@NotNull
	@ApiModelProperty("用户ID")
	private Integer id;

	@NotBlank
	@Length(min = 32, max = 32)
	@ApiModelProperty("密码")
	private String password;

}