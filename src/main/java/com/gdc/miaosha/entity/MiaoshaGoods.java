package com.gdc.miaosha.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 秒杀商品表
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
@Data
@Accessors(chain = true)
@ApiModel(value="MiaoshaGoods对象", description="秒杀商品表")
@TableName("miaosha_goods")
public class MiaoshaGoods {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "商品ID")
    private Integer goodsId;

    @ApiModelProperty(value = "秒杀价")
    private BigDecimal miaoshaPrice;

    @ApiModelProperty(value = "库存数星")
    private Integer stockCount;

    @ApiModelProperty(value = "秒杀开始时间")
    private LocalDateTime startDate;

    @ApiModelProperty(value = "秒杀结束时间")
    private LocalDateTime endDate;

    @ApiModelProperty(value = "是否删除，0否，1是")
    @TableField(fill = FieldFill.INSERT)
    private Integer delFlag;

    @ApiModelProperty(value = "创建人")
    private Integer createBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改人")
    private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

}
