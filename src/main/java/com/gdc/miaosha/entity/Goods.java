package com.gdc.miaosha.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="Goods对象", description="商品表")
@TableName("goods")
public class Goods extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品标题")
    private String goodsTitle;

    @ApiModelProperty(value = "商品的图片")
    private String goodsImg;

    @ApiModelProperty(value = "商品的详情介绍")
    private String goodsDetail;

    @ApiModelProperty(value = "商品单价")
    private BigDecimal goodsPrice;

    @ApiModelProperty(value = "商品库存,-1表示没有限制")
    private Integer goodsStock;

}
