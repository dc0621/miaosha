package com.gdc.miaosha.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderInfo对象", description="订单表")
@TableName("order_info")
public class OrderInfo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户ID")
    private Integer userId;

    @ApiModelProperty(value = "商品ID")
    private Integer goodsId;

    @ApiModelProperty(value = "收获地址lD")
    private Integer deliveryAddrId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品数量")
    private Integer goodsCount;

    @ApiModelProperty(value = "商品单价")
    private BigDecimal goodsPrice;

    @ApiModelProperty(value = "1pc,2android,3ios")
    private Integer orderChannel;

    @ApiModelProperty(value = "订单状态,0新建未支付,1已支付,2已发货,3已收货,4已退款,5已完成")
    private Integer status;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payDate;

    @ApiModelProperty(value = "是否删除，0否，1是")
    private Integer delFlag;

    @ApiModelProperty(value = "创建人")
    private Integer createBy;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改人")
    private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

}
