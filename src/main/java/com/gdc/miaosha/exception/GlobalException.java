package com.gdc.miaosha.exception;

import com.gdc.miaosha.result.CodeMsg;

/**
 * Description: 自定义全局异常
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
public class GlobalException extends RuntimeException{

    /**
     * 状态码信息
     */
    private CodeMsg codeMsg;

    public GlobalException(CodeMsg codeMsg) {
        this.codeMsg = codeMsg;
    }

    public CodeMsg getCodeMsg() {
        return codeMsg;
    }
}
