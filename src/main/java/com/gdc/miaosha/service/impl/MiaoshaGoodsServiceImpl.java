package com.gdc.miaosha.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gdc.miaosha.entity.MiaoshaGoods;
import com.gdc.miaosha.mapper.MiaoshaGoodsMapper;
import com.gdc.miaosha.service.IMiaoshaGoodsService;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 秒杀商品表 服务实现类
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
@Service
public class MiaoshaGoodsServiceImpl extends ServiceImpl<MiaoshaGoodsMapper, MiaoshaGoods> implements IMiaoshaGoodsService {

    @Override
    public Boolean reduceStock(@NotNull Integer goodsId) {
        return baseMapper.reduceStock(goodsId) > 0;
    }
}
