package com.gdc.miaosha.service.impl;

import com.gdc.miaosha.entity.OrderInfo;
import com.gdc.miaosha.mapper.OrderInfoMapper;
import com.gdc.miaosha.service.IOrderInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IOrderInfoService {

}
