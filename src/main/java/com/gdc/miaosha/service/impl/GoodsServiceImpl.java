package com.gdc.miaosha.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gdc.miaosha.entity.Goods;
import com.gdc.miaosha.mapper.GoodsMapper;
import com.gdc.miaosha.service.IGoodsService;
import com.gdc.miaosha.vo.GoodsVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

    @Override
    public List<GoodsVO> listGoodsVO() {
        return baseMapper.listGoodsVO();
    }

    @Override
    public GoodsVO getGoodsVoById(Integer id) {
        return baseMapper.getGoodsVoById(id);
    }
}
