package com.gdc.miaosha.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.gdc.miaosha.constants.RedisConstant;
import com.gdc.miaosha.entity.MiaoshaOrder;
import com.gdc.miaosha.entity.OrderInfo;
import com.gdc.miaosha.entity.User;
import com.gdc.miaosha.exception.GlobalException;
import com.gdc.miaosha.result.CodeMsg;
import com.gdc.miaosha.service.IMiaoShaService;
import com.gdc.miaosha.service.IMiaoshaGoodsService;
import com.gdc.miaosha.service.IMiaoshaOrderService;
import com.gdc.miaosha.service.IOrderInfoService;
import com.gdc.miaosha.util.RedisUtil;
import com.gdc.miaosha.vo.GoodsVO;
import com.google.common.base.Throwables;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Random;

import static com.gdc.miaosha.constants.OrderConstant.*;

/**
 * Description: 秒杀 Service 实现
 * @author: gdc
 * @date: 2022/4/5
 * @version 1.0
 */
@Slf4j
@Service
public class MiaoShaServiceImpl implements IMiaoShaService {

    /**
     * 验证码操作符号
     */
    private static final char[] ops = new char[] {'+', '-', '*'};

    @Autowired
    private IMiaoshaGoodsService miaoshaGoodsService;
    @Autowired
    private IOrderInfoService orderService;
    @Autowired
    private IMiaoshaOrderService miaoshaOrderService;

    @Transactional
    @Override
    public OrderInfo createMiaoShaOrder(User user, GoodsVO goods) {
        // step 1 减库存
        Boolean reduceStockRes = miaoshaGoodsService.reduceStock(goods.getId());
        if (!reduceStockRes) {
            setMiaoShaGoodsOver(goods.getId());
            throw new GlobalException(CodeMsg.MIAO_SHA_OVER);
        }

        // step 2 保存订单
        OrderInfo order = new OrderInfo()
                .setUserId(user.getId())
                .setGoodsId(goods.getId())
                .setDeliveryAddrId(0)
                .setGoodsName(goods.getGoodsName())
                .setGoodsCount(1)
                .setGoodsPrice(goods.getMiaoshaPrice())
                .setOrderChannel(ORDER_CHANNEL_PC)
                .setStatus(STATUS_NOT_PAY)
                .setPayDate(LocalDateTime.now())
                .setCreateBy(user.getId())
                .setCreateTime(LocalDateTime.now());
        orderService.save(order);

        // step 3 保存秒杀订单
        MiaoshaOrder miaoshaOrder = new MiaoshaOrder()
                .setGoodsId(goods.getId())
                .setOrderId(order.getId())
                .setUserId(user.getId());
        miaoshaOrderService.save(miaoshaOrder);

        // step 4 订单保存成功后，写入到Redis，提升订单查询效率
        RedisUtil.set(RedisConstant.MIAOSHA_ORDER_USER_GOODS + user.getId() + "_" + goods.getId(), JSON.toJSONString(miaoshaOrder));
        return order;
    }

    @Override
    public Integer getResult(@NotNull Integer userId, @NotNull Integer goodsId) {
        MiaoshaOrder order = miaoshaOrderService.getByUserIdAndGoodsId(userId, goodsId);
        if(Objects.nonNull(order)) {        //秒杀成功
            return order.getOrderId();
        }else {
            return getMiaoShaGoodsOver(goodsId) ? MIAOSHA_GOODS_STATUS_FINISH : MIAOSHA_GOODS_STATUS_IN_PROGRESS;
        }
    }

    @Override
    public String createMiaoShaPath(@NotNull Integer userId, @NotNull Integer goodsId) {
        if(Objects.isNull(userId) || goodsId <=0) {
            return null;
        }

        String result = SecureUtil.md5(IdUtil.simpleUUID());
        RedisUtil.set(RedisConstant.MIAOSHA_PATH + userId + "_"+ goodsId, result);
        return result;
    }

    @Override
    public Boolean checkPath(@NotNull Integer userId, @NotNull Integer goodsId, @NotBlank String path) {
        String pathOld = RedisUtil.getString(RedisConstant.MIAOSHA_PATH + userId + "_"+ goodsId);
        return Objects.equals(path, pathOld);
    }

    @Override
    public Boolean checkVerifyCode(@NotNull Integer userId, @NotNull Integer goodsId, int verifyCode) {
        if(goodsId <= 0) {
            return false;
        }
        Integer codeOld = RedisUtil.get(RedisConstant .MIAOSHA_VERIFYCODE + userId + "_" + goodsId, Integer.class);
        if(Objects.isNull(codeOld) || codeOld != verifyCode) {
            return false;
        }
        RedisUtil.del(RedisConstant .MIAOSHA_VERIFYCODE + userId + "_" + goodsId);
        return true;
    }

    @Override
    public BufferedImage createVerifyCode(@NotNull Integer userId, @NotNull Integer goodsId) {
        int width = 80;
        int height = 32;
        //create the image
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        // set the background color
        g.setColor(new Color(0xDCDCDC));
        g.fillRect(0, 0, width, height);
        // draw the border
        g.setColor(Color.black);
        g.drawRect(0, 0, width - 1, height - 1);
        // create a random instance to generate the codes
        Random rdm = new Random();
        // make some confusion
        for (int i = 0; i < 50; i++) {
            int x = rdm.nextInt(width);
            int y = rdm.nextInt(height);
            g.drawOval(x, y, 0, 0);
        }
        // generate a random code
        String verifyCode = generateVerifyCode(rdm);
        g.setColor(new Color(0, 100, 0));
        g.setFont(new Font("Candara", Font.BOLD, 24));
        g.drawString(verifyCode, 8, 24);
        g.dispose();
        //把验证码存到redis中
        int rnd = calc(verifyCode);
        RedisUtil.set(RedisConstant.MIAOSHA_VERIFYCODE + userId + "_" + goodsId, rnd, RedisConstant.EXPIRED_1_MINUTE);
        //输出图片
        return image;
    }

    /**
     * 设置秒杀商品结束标识
     *
     * @param goodsId 商品id
     */
    private void setMiaoShaGoodsOver(Integer goodsId) {
        RedisUtil.set(RedisConstant.MIAOSHA_GOODS_STOCK_OVER + goodsId, true);
    }

    /**
     * 获取秒杀商品库存是否已经结束
     *
     * @param goodsId 商品id
     * @return boolean
     */
    private boolean getMiaoShaGoodsOver(Integer goodsId) {
        return RedisUtil.exists(RedisConstant.MIAOSHA_GOODS_STOCK_OVER + goodsId);
    }

    /**
     * 计算
     *
     * @param exp 表达式
     * @return      结果
     */
    private static int calc(String exp) {
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("JavaScript");
            return (Integer)engine.eval(exp);
        }catch(Exception e) {
            log.error("JS引擎解析计算验证码值异常，异常原因为：{}", Throwables.getStackTraceAsString(e));
            return 0;
        }
    }

    /**
     * 生成验证码内容
     */
    private String generateVerifyCode(Random rdm) {
        int num1 = rdm.nextInt(10);
        int num2 = rdm.nextInt(10);
        int num3 = rdm.nextInt(10);
        char op1 = ops[rdm.nextInt(3)];
        char op2 = ops[rdm.nextInt(3)];
        return  "" + num1 + op1 + num2 + op2 + num3;
    }
}
