package com.gdc.miaosha.service;

import com.gdc.miaosha.entity.MiaoshaGoods;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 秒杀商品表 服务类
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface IMiaoshaGoodsService extends IService<MiaoshaGoods> {

    /**
     * 扣减商品库存
     * @param goodsId       商品ID
     * @return              结果
     */
    Boolean reduceStock(@NotNull Integer goodsId);
}
