package com.gdc.miaosha.service;

import com.gdc.miaosha.entity.MiaoshaOrder;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 秒杀订单表 服务类
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface IMiaoshaOrderService extends IService<MiaoshaOrder> {


    /**
     * 通过用户id和商品id，获取秒杀订单数量
     *
     * @param userId  用户id
     * @param goodsId 商品id
     * @return {@link Long}
     */
    Integer countByUserIdAndGoodsId(@NotNull Integer userId, @NotNull Integer goodsId);

    /**
     * 通过用户id和产品id获取秒杀订单
     *
     * @param userId  用户id
     * @param goodsId 商品id
     * @return {@link MiaoshaOrder}
     */
    MiaoshaOrder getByUserIdAndGoodsId(@NotNull Integer userId, @NotNull Integer goodsId);
}
