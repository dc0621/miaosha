package com.gdc.miaosha.service;

import com.gdc.miaosha.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gdc.miaosha.vo.GoodsVO;

import java.util.List;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface IGoodsService extends IService<Goods> {

    /**
     * 获取商品列表
     * @return          商品列表
     */
    List<GoodsVO> listGoodsVO();

    /**
     * 通过商品ID, 获取商品信息
     * @param id        商品ID
     * @return          商品信息
     */
    GoodsVO getGoodsVoById(Integer id);
}
