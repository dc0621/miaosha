package com.gdc.miaosha.service;

import com.gdc.miaosha.entity.OrderInfo;
import com.gdc.miaosha.entity.User;
import com.gdc.miaosha.vo.GoodsVO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.awt.image.BufferedImage;

/**
 * Description: 秒杀 Service
 * @author: gdc
 * @date: 2022/4/5
 * @version 1.0
 */
public interface IMiaoShaService {


    /**
     * 创建秒杀订单
     *
     * @param user  用户
     * @param goods 商品信息
     * @return {@link OrderInfo}
     */
    OrderInfo createMiaoShaOrder(User user, GoodsVO goods);

    /**
     * 获取秒杀结果
     *
     * @param userId  用户ID
     * @param goodsId 商品ID
     * @return {@link Integer}
     */
    Integer getResult(@NotNull Integer userId, @NotNull Integer goodsId);

    /**
     * 创建秒杀Path
     *
     * @param userId  用户id
     * @param goodsId 商品id
     * @return {@link String}
     */
    String createMiaoShaPath(@NotNull Integer userId, @NotNull Integer goodsId);

    /**
     * 验证秒杀Path
     *
     * @param userId  用户id
     * @param goodsId 商品id
     * @param path    路径
     * @return {@link Boolean}
     */
    Boolean checkPath(@NotNull Integer userId, @NotNull Integer goodsId, @NotBlank String path);

    /**
     * 检查验证码
     *
     * @param userId     用户id
     * @param goodsId    商品id
     * @param verifyCode 验证码
     * @return {@link Boolean}
     */
    Boolean checkVerifyCode(@NotNull Integer userId, @NotNull Integer goodsId, int verifyCode);

    /**
     * 创建验证码
     *
     * @param userId  用户id
     * @param goodsId 商品id
     * @return {@link BufferedImage}
     */
    BufferedImage createVerifyCode(@NotNull Integer userId, @NotNull Integer goodsId);
}
