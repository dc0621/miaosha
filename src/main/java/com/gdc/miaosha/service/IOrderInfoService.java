package com.gdc.miaosha.service;

import com.gdc.miaosha.entity.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface IOrderInfoService extends IService<OrderInfo> {

}
