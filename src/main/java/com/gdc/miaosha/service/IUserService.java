package com.gdc.miaosha.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gdc.miaosha.entity.User;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 登陆用户表 服务类
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface IUserService extends IService<User> {

    /**
     * 通过手机号获取用户信息
     * @param mobile        手机号
     * @return              用户信息
     */
    User getByMobile(String mobile);

    /**
     * 通过Token获取用户
     *
     * @param token 令牌
     * @return {@link User}
     */
    User getByToken(@NotBlank String token);

    /**
     * 通过用户ID获取用户信息
     * @param id            用户ID
     * @return              用户信息
     */
    User getById(Integer id);

}
