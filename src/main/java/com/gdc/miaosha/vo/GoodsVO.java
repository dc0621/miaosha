package com.gdc.miaosha.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Description: 商品VO
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
@Data
@ApiModel(value="GoodsVO", description="商品VO")
public class GoodsVO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    private Integer id;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品标题")
    private String goodsTitle;

    @ApiModelProperty(value = "商品的图片")
    private String goodsImg;

    @ApiModelProperty(value = "商品的详情介绍")
    private String goodsDetail;

    @ApiModelProperty(value = "商品单价")
    private BigDecimal goodsPrice;

    @ApiModelProperty(value = "商品库存,-1表示没有限制")
    private Integer goodsStock;

    @ApiModelProperty(value = "秒杀价")
    private BigDecimal miaoshaPrice;

    @ApiModelProperty(value = "库存数星")
    private Integer stockCount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "秒杀开始时间")
    private LocalDateTime startDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "秒杀结束时间")
    private LocalDateTime endDate;

    @ApiModelProperty(value = "是否删除，0否，1是")
    private Integer delFlag;

    @ApiModelProperty(value = "创建人")
    private Integer createBy;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改人")
    private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

}
