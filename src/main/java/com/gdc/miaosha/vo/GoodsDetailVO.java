package com.gdc.miaosha.vo;

import com.gdc.miaosha.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Description: 商品详情VO
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
@Data
@Accessors(chain = true)
@ApiModel(value="GoodsDetailVO", description="商品详情VO")
public class GoodsDetailVO {

	@ApiModelProperty(value = "秒杀状态")
	private Integer miaoshaStatus;

	@ApiModelProperty(value = "剩余时间")
	private Long remainSeconds;

	@ApiModelProperty(value = "商品信息")
	private GoodsVO goods;

	@ApiModelProperty(value = "用户信息")
	private User user;

}