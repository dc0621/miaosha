package com.gdc.miaosha.vo;

import com.gdc.miaosha.entity.OrderInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Description: 订单VO
 * @author: gdc
 * @date: 2022/4/6
 * @version 1.0
 */
@Data
@AllArgsConstructor
@ApiModel(value="OrderDetailVO", description="订单详情VO")
public class OrderDetailVO {

	@ApiModelProperty(value = "订单")
	private OrderInfo order;

	@ApiModelProperty(value = "商品")
	private GoodsVO goods;

}