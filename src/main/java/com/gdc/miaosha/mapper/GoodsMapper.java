package com.gdc.miaosha.mapper;

import com.gdc.miaosha.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gdc.miaosha.vo.GoodsVO;

import java.util.List;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    /**
     * 获取商品列表
     * @return          商品列表
     */
    List<GoodsVO> listGoodsVO();

    /**
     * 通过商品ID, 获取商品信息
     * @param id        商品ID
     * @return          商品信息
     */
    GoodsVO getGoodsVoById(Integer id);
}
