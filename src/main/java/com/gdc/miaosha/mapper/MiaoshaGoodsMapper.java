package com.gdc.miaosha.mapper;

import com.gdc.miaosha.entity.MiaoshaGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 秒杀商品表 Mapper 接口
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface MiaoshaGoodsMapper extends BaseMapper<MiaoshaGoods> {

    /**
     * 扣减库存
     * @param goodsId           商品ID
     * @return                  更新数量
     */
    Integer reduceStock(Integer goodsId);
}
