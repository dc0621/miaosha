package com.gdc.miaosha.mapper;

import com.gdc.miaosha.entity.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}
