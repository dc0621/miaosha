package com.gdc.miaosha.mapper;

import com.gdc.miaosha.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 登陆用户表 Mapper 接口
 * </p>
 *
 * @author gdc
 * @since 2022-04-04
 */
public interface UserMapper extends BaseMapper<User> {

}
