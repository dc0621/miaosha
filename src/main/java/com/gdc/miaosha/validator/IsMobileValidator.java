package com.gdc.miaosha.validator;

import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.StrUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Description: 是否为手机号验证器
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
public class IsMobileValidator implements ConstraintValidator<IsMobile, String> {

    /**
     * 是否必传标识
     */
    private boolean required;

    @Override
    public void initialize(IsMobile constraintAnnotation) {
        required =constraintAnnotation.require();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (required) {
            return PhoneUtil.isMobile(value);
        } else {
            if (StrUtil.isEmpty(value)) {
                return true;
            } else {
                return PhoneUtil.isMobile(value);
            }
        }
    }
}
