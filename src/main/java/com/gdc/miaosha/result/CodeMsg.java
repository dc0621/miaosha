package com.gdc.miaosha.result;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Description: 状态码 Enum
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
@AllArgsConstructor
@Getter
public enum CodeMsg {

	/**
	 * 通用错误码
	 */
	SUCCESS(0, "success"),
	SERVER_ERROR(500100, "服务端异常"),
	BIND_ERROR(500101, "参数校验异常：%s"),
	REQUEST_ILLEGAL(500102, "请求非法"),
	ACCESS_LIMIT_REACHED(500104, "访问太频繁！"),

	/**
	 * 登录模块 5002XX
	 */
	SESSION_ERROR(500210, "Session不存在或者已经失效"),
	PASSWORD_EMPTY(500211, "登录密码不能为空"),
	MOBILE_EMPTY(500212, "手机号不能为空"),
	MOBILE_ERROR(500213, "手机号格式错误"),
	MOBILE_NOT_EXIST(500214, "手机号不存在"),
	PASSWORD_ERROR(500215, "密码错误"),
	PASSWORD_MODIFY_FAIL(500216, "密码修改失败"),
	VERIFYCODE_ERROR(500217, "验证码错误"),
	VERIFYCODE_NOT_EXIST(500218, "验证码不能为空"),

	//商品模块 5003XX

	//订单模块 5004XX
	ORDER_NOT_EXIST(500400, "订单不存在"),

	//秒杀模块 5005XX
	MIAO_SHA_OVER(500500, "商品已经秒杀完毕"),
	MIAO_SHA_REPEATE(500501, "不能重复秒杀"),
	MIAOSHA_FAIL(500502, "秒杀失败"),

	;


	@ApiModelProperty(value = "错误编码")
	private int code;

	@ApiModelProperty(value = "错误信息")
	private String msg;

	public CodeMsg fillargs(Object... args) {
		String message = String.format(this.msg, args);
		return BIND_ERROR;
	}

}
