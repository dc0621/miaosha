package com.gdc.miaosha.result;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

/**
 * Description: 公共返回结果类
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
@Getter
public class Result<T> {

	@ApiModelProperty(value = "状态码")
	private int code;

	@ApiModelProperty(value = "状态信息")
	private String msg;

	@ApiModelProperty(value = "返回数据")
	private T data;

	/**
	 * 成功结果调用
	 */
	public static <T> Result<T> success(){
		return new Result<>();
	}

	/**
	 * 成功结果调用
	 * @param data		数据
	 */
	public static <T> Result<T> success(T data){
		return new Result<>(data);
	}
	
	/**
	 * 失败结果调用
	 * @param codeMsg   状态信息
	 */
	public static <T> Result<T> error(CodeMsg codeMsg){
		return new Result<>(codeMsg);
	}

	/**
	 * 失败结果调用
	 * @param code		状态码
	 * @param msg		状态信息
	 */
	public static <T> Result<T> error(int code, String msg){
		return new Result<>(code, msg);
	}

	public Result() {
		this.code = CodeMsg.SUCCESS.getCode();
		this.msg = CodeMsg.SUCCESS.getMsg();
	}

	private Result(T data) {
		this.code = CodeMsg.SUCCESS.getCode();
		this.msg = CodeMsg.SUCCESS.getMsg();
		this.data = data;
	}
	
	private Result(CodeMsg codeMsg) {
		if(codeMsg == null) {
			return;
		}
		this.code = codeMsg.getCode();
		this.msg = codeMsg.getMsg();
	}

	public Result(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

}
