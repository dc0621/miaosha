package com.gdc.miaosha.config;

import com.gdc.miaosha.entity.User;

/**
 * Description: 用户上下文
 * @author: gdc
 * @date: 2022/4/14
 * @version 1.0
 */
public class UserContext {
	
	private static ThreadLocal<User> userHolder = new ThreadLocal<>();
	
	public static void setUser(User user) {
		userHolder.set(user);
	}
	
	public static User getUser() {
		return userHolder.get();
	}

}
