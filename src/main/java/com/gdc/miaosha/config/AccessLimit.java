package com.gdc.miaosha.config;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Description: 访问限流注解
 * @author: gdc
 * @date: 2022/4/13
 * @version 1.0
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface AccessLimit {

	/**
	 * 秒数
	 */
	int seconds();

	/**
	 * 最大访问次数
	 */
	int maxCount();

	/**
	 * 是否需要登陆,默认需要
	 */
	boolean needLogin() default true;
}
