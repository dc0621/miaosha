package com.gdc.miaosha.constants;

/**
 * Description: 公共常量
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
public class CommonConstant {

    /**
     * 是否删除，0否，1是
     */
    public static final Integer DEL_FLAG_N = 0;
    public static final Integer DEL_FLAG_Y = 1;


    /**
     * cookie Key
     */
    public static final String COOKIE_KEY = "tk";
}
