package com.gdc.miaosha.constants;

/**
 * Description: Redis常量
 * @author: gdc
 * @date: 2022/4/4
 * @version 1.0
 */
public class RedisConstant {

    /**
     * token前缀
     */
    public static final String TOKEN_PREFIX = "token:";

    /**
     * 过期时长，2天
     */
    public static final Integer EXPIRED_2_DAY = 2 * 24 * 60 * 60;
    /**
     * 过期时长，1分钟
     */
    public static final Integer EXPIRED_1_MINUTE = 60;


    /**
     * 商品列表页面
     */
    public static final String GOODS_LIST_PAGE = "goodsListPage";
    /**
     * 商品详情页面
     */
    public static final String GOODS_DETAIL_PAGE = "goodsDetailPage:";

    /**
     * 用户详情
     */
    public static final String USER = "user:";

    /**
     * 秒杀订单_用户商品
     */
    public static final String MIAOSHA_ORDER_USER_GOODS = "miaoShaOrderUserGoods:";

    /**
     * 秒杀商品库存
     */
    public static final String MIAOSHA_GOODS_STOCK = "miaoShaGoodsStock:";

    /**
     * 商品库存用完
     */
    public static final String MIAOSHA_GOODS_STOCK_OVER = "miaoShaGoodsStockOver:";

    /**
     * 秒杀地址
     */
    public static final String MIAOSHA_PATH = "miaoShaPath:";

    /**
     * 秒杀验证码
     */
    public static final String MIAOSHA_VERIFYCODE = "miaoShaVerifyCode:";

    /**
     * 访问限制
     */
    public static final String ACCESS_LIMIT = "accessLimit:";

}
