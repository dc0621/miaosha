package com.gdc.miaosha.constants;

/**
 * Description: 订单常量
 * @author: gdc
 * @date: 2022/4/5
 * @version 1.0
 */
public class OrderConstant {

    /**
     * 订单状态,0新建未支付,1已支付,2已发货,3已收货,4已退款,5已完成
     */
    public static final Integer STATUS_NOT_PAY = 0;
    public static final Integer STATUS_PAY = 1;
    public static final Integer STATUS_DELIVERY = 2;
    public static final Integer STATUS_RECEIVE = 3;
    public static final Integer STATUS_REFUND = 4;
    public static final Integer STATUS_FINISH = 5;


    /**
     * 订单渠道,1pc,2android,3ios
     */
    public static final Integer ORDER_CHANNEL_PC = 1;
    public static final Integer ORDER_CHANNEL_ANDROID = 2;
    public static final Integer ORDER_CHANNEL_IOS = 3;

    /**
     * 秒杀商品状态,-1结束,0进行中
     */
    public static final Integer MIAOSHA_GOODS_STATUS_FINISH = -1;
    public static final Integer MIAOSHA_GOODS_STATUS_IN_PROGRESS = 0;

}
